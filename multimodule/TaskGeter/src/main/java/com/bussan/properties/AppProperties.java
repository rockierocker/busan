/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bussan.properties;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 *
 * @author Administrator
 */
public class AppProperties extends Properties{
    public AppProperties() throws FileNotFoundException, IOException{
        File child =  new File(System.getProperty("user.dir"));
        InputStream inputStream = new FileInputStream(new File(child.getParent()+"/TaskGeterFolder/app.properties"));
        this.load(inputStream);
    }
}
