/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springfive.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import javax.servlet.ServletContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Administrator
 */
@RestController
@RequestMapping("api/formater")
public class ConverterController {
    @Autowired ServletContext servletContext;
    
    @RequestMapping(value="/bca", method=RequestMethod.POST)
    public @ResponseBody HashMap<String,Object> bca(@RequestParam("file") MultipartFile file){
        HashMap<String,Object> map = new HashMap<>();
        String path = servletContext.getRealPath("resources");
//            if (!file.getOriginalFilename().isEmpty()) {
//                BufferedOutputStream outputStream = new BufferedOutputStream(
//                   new FileOutputStream( new File(path,fileName)));
//                outputStream.write(file.getBytes());
//                outputStream.flush();
//                outputStream.close();
//                map.put("status",true);
//                map.put("message","Foto berhasil di upload");
//                map.put("Result","busana/"+fileName);
//                map.put("fotoId",currentTime);
//             }
            map.put(path, file.getName());
            return map;
        }
    
}
