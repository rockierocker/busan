/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springfive.controller;

import com.bussan.taskgeter.TaskGeter;
import java.util.HashMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Administrator
 */
@RestController
@RequestMapping("/")
public class MainController {
    @RequestMapping(value="/", method=RequestMethod.GET)
    public @ResponseBody HashMap<String,Object> get(){
        HashMap<String,Object> map = new HashMap<>();
        map.put("Name",new TaskGeter().bcaFormat());
        map.put("Version", "0.1");
        return map;
    }
}
