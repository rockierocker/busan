/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springfive.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 *
 * @author User
 */
@PropertySource("classpath:app.properties")
@Component
public class StartUpApplication {
    @Autowired
    Environment env;
    @EventListener(ContextRefreshedEvent.class)
    void contextRefreshedEvent() {
        
    }
    
    
}
