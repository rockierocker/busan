/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.formater.format;

/**
 *
 * @author Administrator
 */
public class Footer {
    private Double saldoAwal,saldoAkhir,mutasiDebet,mutasiKredit;

    public Double getSaldoAwal() {
        return saldoAwal;
    }

    public void setSaldoAwal(Double saldoAwal) {
        this.saldoAwal = saldoAwal;
    }

    public Double getSaldoAkhir() {
        return saldoAkhir;
    }

    public void setSaldoAkhir(Double saldoAkhir) {
        this.saldoAkhir = saldoAkhir;
    }

    public Double getMutasiDebet() {
        return mutasiDebet;
    }

    public void setMutasiDebet(Double mutasiDebet) {
        this.mutasiDebet = mutasiDebet;
    }

    public Double getMutasiKredit() {
        return mutasiKredit;
    }

    public void setMutasiKredit(Double mutasiKredit) {
        this.mutasiKredit = mutasiKredit;
    }
    
}
