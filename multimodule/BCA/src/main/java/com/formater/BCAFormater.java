package com.formater;

import com.formater.format.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Administrator
 */
public class BCAFormater {
    private Footer footer;
    private Body body;
    private Header header;
    private ArrayList<Body> listBody;
    private File file;
    
    private String data;
    public BCAFormater(byte[] streamFile){
        footer = null;
        body =  null;
        header = null;
        listBody = null;
        data = new String(streamFile);
    }

    public BCAFormater(File file) throws FileNotFoundException, IOException{
        footer = null;
        body =  null;
        header = null;
        listBody = null;
        FileInputStream fos = new FileInputStream(file);
        byte [] streamFile = new byte[(int)file.length()];
        fos.read(streamFile);
        fos.close();
        data = new String(streamFile);
        this.file = file;
    }
    
    public BCAFormater(String data){
        footer = null;
        body =  null;
        header = null;
        listBody = null;
        this.data =  data;
    }
    
   
    public Footer getFooter() {
        return footer;
    }

    public void setFooter(Footer footer) {
        this.footer = footer;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public ArrayList<Body> getListBody() {
        return listBody;
    }

    public void setListBody(ArrayList<Body> listBody) {
        this.listBody = listBody;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    
    
    public void runFormater() throws FileNotFoundException, IOException, ParseException, Exception{
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat sdfBody = new SimpleDateFormat("dd/MM");
        
        BufferedReader b = new BufferedReader(new FileReader(file));
        String readLine = "";
        header = new Header();
        body = new Body();
        footer = new Footer();
        listBody = new ArrayList<>();
        int i=0;
        int j=0;
        while ((readLine = b.readLine()) != null) {
            //Header Logic
            if(i==0){
               header.setTitle(readLine.substring(0, readLine.indexOf(",")));
            } else if(i==2){
               header.setNoRekening((readLine.split(":")[1]).replaceAll("\"", "").trim());
            }else if(i==3){
               header.setName((readLine.split(":")[1]).replaceAll("\"", "").trim());
            }else if(i==4){
               String periode = (readLine.split(":")[1]).replaceAll("\"", "").trim();
               header.setPeriodeAwal(sdf.parse(periode.split("-")[0].trim()));
               header.setPeriodeAkhir(sdf.parse(periode.split("-")[1].trim()));
            }else if(i==5){
               header.setKodeMataUang((readLine.split(":")[1]).replaceAll("\"", "").trim());
            }
            
            //Body Logic
            if(i>6){
                String[] data = readLine.split("\\\",");
                if(data.length==5){
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(header.getPeriodeAwal());
                    //body.setTanggalTransaksi(sdf.parse(data[0].replaceAll("\"","").trim()+"/"+cal.get(Calendar.YEAR)));
                    body.setTanggalTransaksi(sdfBody.parse(data[0].replaceAll("\"","").trim()));
                    body.setKeterangan(data[1].replaceAll("\"","").trim());
                    body.setCabang(Integer.parseInt(data[2].replaceAll("\"","").trim()));
                    body.setJumlah(data[3].replaceAll("\"","").trim());
                    body.setSaldo(Double.parseDouble(data[4].replaceAll("\"","").replaceAll("\\,","").trim()));
                    System.out.println(readLine);
                    listBody.add(body);
                }else{
                    //Footer Logic
                    if(j==0){
                        footer.setSaldoAwal(Double.parseDouble(((readLine.split("\\\",")[0]).split(":")[1]).replaceAll("\"", "").replaceAll("\\,", "").trim()));
                    }else if(j==1){
                        footer.setMutasiDebet(Double.parseDouble(((readLine.split("\\\",")[0]).split(":")[1]).replaceAll("\"", "").replaceAll("\\,", "").trim()));
                    }else if(j==2){
                        footer.setMutasiKredit(Double.parseDouble(((readLine.split("\\\",")[0]).split(":")[1]).replaceAll("\"", "").replaceAll("\\,", "").trim()));
                    }else if(j==3){
                        footer.setSaldoAkhir(Double.parseDouble(((readLine.split("\\\",")[0]).split(":")[1]).replaceAll("\"", "").replaceAll("\\,", "").trim()));
                    }
                    j++;
                }
                
            }
            i++;
        }
    }
    
    
}
